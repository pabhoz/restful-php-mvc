<?php
/**
 * Description of cUrlClient
 *
 * @author pablo bejarano
 * @pabhoz on twitter
 */
class cUrlClient {
    
    function __construct($url,$rt = 1,$jsonO = false) {
        
        $this->url = $url;
        $this->returnTransfer = $rt;
        $this->jsonOutput = $jsonO;
        
    }
    
    function execute(){
        
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $this->url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, $this->returnTransfer); 
        if($this->jsonOutput == false){
            $output = json_decode(curl_exec($ch),true);
        }else{
            $output = curl_exec($ch);
        }
        curl_close($ch);
        
        return $output;
    }
}

?>
